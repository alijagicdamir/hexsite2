async function fetchData(){
    let response = await fetch("https://api.nomics.com/v1/currencies/ticker?key=2ae62545bbe20a35fc20f2a7cc5a95a9&ids=HEX&interval=1d,30d&convert=USD&per-page=100&page=1");
    let data = await response.json();
    data = JSON.stringify(data);
    data = JSON.parse(data);

    $('#count0').text(data[0].price.substring(0,5))
    $('#count1').text(Math.round(data[0].market_cap / 1_000_000_000))
}

$('#count2').text('62')
$('#count3').text('239,401')

fetchData();